package com.jieaudio.impl;

import android.media.MediaPlayer;
import android.util.Log;

import com.jieaudio.listeners.OnPlayStatusListener;

import static android.content.ContentValues.TAG;

/**
 * Description:空实现
 * Date：2018/12/19-17:32
 * Author: Liangchaojie
 */
public class EmptyPlayStatusListener implements OnPlayStatusListener {
    @Override
    public void onPrepared(MediaPlayer mp) {

    }

    @Override
    public void onCompletion(MediaPlayer mp) {

    }

    @Override
    public void isPlaying() {

    }



    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {

    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }

    @Override
    public void onCodeErrorException(String exception) {
        Log.i(TAG, "JiePlayer::onCodeErrorException: "+exception);
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        return false;
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void wantPlay() {

    }
}
