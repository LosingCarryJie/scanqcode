package com.jieaudio.listeners;

import android.media.AudioManager;
import android.util.Log;

/**
 * Description:
 * Date：2018/12/19-16:11
 * Author: Liangchaojie
 */
public class OnJieAudioFocusChangeListener implements AudioManager.OnAudioFocusChangeListener {
    @Override
    public void onAudioFocusChange(int focusChange) {
        Log.i("jie", "jie onAudioFocusChange: ");
    }
}
