package com.jieaudio.listeners;

import android.content.Context;

/**
 * Description:音频播放器应该具备的属性
 * Date：2018/12/19-17:28
 * Author: Liangchaojie
 */
public interface OnPlayerListener {
    void init(Context context);
    void audioGetFocus();
    void audioRealeaseFocus();
    boolean isPlaying();
    void start(String url,OnPlayStatusListener listener);
    void pause();
    void resume();
    boolean hasPause();
    void wantPlay();
    void wantPause();
    void destory();
}
