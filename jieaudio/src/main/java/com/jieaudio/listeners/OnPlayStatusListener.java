package com.jieaudio.listeners;

import android.media.MediaPlayer;

/**
 * Description: JieAudio播放音频过程状态监听
 * Date：2018/12/19-16:47
 * Author: Liangchaojie
 */
public interface OnPlayStatusListener {
    void onPrepared(MediaPlayer mp);
    void onCompletion(MediaPlayer mp);
    void isPlaying();
    void onBufferingUpdate(MediaPlayer mp, int percent);
    boolean onError(MediaPlayer mp, int what, int extra);
    void onCodeErrorException(String exception);//程序员代码造成的错误
    boolean onInfo(MediaPlayer mp, int what, int extra);

    void pause();
    void resume();
    void wantPlay();
}
