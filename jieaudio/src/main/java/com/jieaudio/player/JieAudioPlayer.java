package com.jieaudio.player;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.text.TextUtils;
import android.util.Log;

import com.jieaudio.impl.EmptyPlayStatusListener;
import com.jieaudio.listeners.OnJieAudioFocusChangeListener;
import com.jieaudio.listeners.OnPlayStatusListener;
import com.jieaudio.listeners.OnPlayerListener;
import com.toastutil.toastutil.ToastUtil;

/**
 * Description:播放器用于播放语音
 * Date：2018/12/19-16:59
 * Author: Liangchaojie
 */
public class JieAudioPlayer implements OnPlayerListener {
    private static final JieAudioPlayer ourInstance = new JieAudioPlayer();
    private static final MediaPlayer mediaPlayer = new MediaPlayer();
    private OnJieAudioFocusChangeListener l = new OnJieAudioFocusChangeListener();
    private OnPlayStatusListener mListener;
    private AudioManager mAudioManager;
    public static JieAudioPlayer getInstance() {
        return ourInstance;
    }

    private JieAudioPlayer() {
    }

    private boolean is_pause = false;//是否被暂停过?
    private boolean is_init_playing;
    private boolean is_want_play=true;

    @Override
    public void init(Context context) {
        mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
    }

    @Override
    public void audioGetFocus() {
        mAudioManager.requestAudioFocus(l, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
    }

    @Override
    public void audioRealeaseFocus() {
        mAudioManager.abandonAudioFocus(l);
    }

    @Override
    public boolean isPlaying() {
        return mediaPlayer.isPlaying();
    }

    @Override
    public void start(String url, OnPlayStatusListener listener) {
        if(listener==null) listener = new EmptyPlayStatusListener();
        this.mListener = listener;
        if(TextUtils.isEmpty(url)) {listener.onCodeErrorException("url不能为空!");return;}

        is_init_playing = true;
        mediaPlayer.reset();
        try {
            mediaPlayer.setDataSource(url);
            mListener.wantPlay();
            mediaPlayer.prepareAsync();
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mListener.onPrepared(mp);
                    mediaPlayer.start();
                    audioGetFocus();
                }
            });
            mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                @Override
                public void onBufferingUpdate(MediaPlayer mp, int percent) {
                    mListener.onBufferingUpdate(mp,percent);
                    if(is_init_playing){//如果之前不是播放状态那么现在状态就是播放状态了
                        mListener.isPlaying();
                        is_init_playing = false;
                    }
                }
            });
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mListener.onCompletion(mp);
                }
            });
            mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    return  mListener.onError(mp,what,extra);
                }
            });
            mediaPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                @Override
                public boolean onInfo(MediaPlayer mp, int what, int extra) {
                    return mListener.onInfo(mp,what,extra);
                }
            });
        } catch (Exception e) {
            listener.onCodeErrorException(e.getMessage());
            e.printStackTrace();
        }
    }
    
    @Override
    public void pause() {
        if(isPlaying()){
            mediaPlayer.pause();
            audioRealeaseFocus();
            is_pause = true;
            if(mListener!=null){
                mListener.pause();
            }
        }
    }

    @Override
    public void resume() {
        if(is_pause){
            mediaPlayer.start();
            audioGetFocus();
            is_pause = false;
            if(mListener!=null){
                mListener.resume();
            }
        }
    }

    @Override
    public boolean hasPause() {
        return is_pause;
    }

    @Override
    public void wantPlay() {

    }

    @Override
    public void wantPause() {

    }

    @Override
    public void destory() {
        mediaPlayer.release();
    }
    
}
