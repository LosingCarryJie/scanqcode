package com.simpledemo.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.simpledemo.R;

public class ToastActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
         Toast.makeText(ToastActivity.this,"FileNotFoundException ",Toast.LENGTH_LONG).show();
    }
}
