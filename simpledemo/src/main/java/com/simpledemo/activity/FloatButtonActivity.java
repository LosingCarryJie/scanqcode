package com.simpledemo.activity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.simpledemo.R;
import com.simpledemo.view.MyButton;

public class FloatButtonActivity extends AppCompatActivity {
    private MyButton btn_my;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_float_button);
        btn_my=findViewById(R.id.btn_my);

        RequestOptions requestOptions = new RequestOptions()
                .placeholder(new ColorDrawable(Color.BLACK))
                .error(new ColorDrawable(Color.BLUE))
                .fallback(new ColorDrawable(Color.RED));

        Glide.with(this)
                .load("img")
                .apply(requestOptions);
                //.into(null);
    }
}
