package com.simpledemo.activity;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.jieaudio.impl.EmptyPlayStatusListener;
import com.jieaudio.listeners.OnJieAudioFocusChangeListener;
import com.jieaudio.listeners.OnPlayStatusListener;
import com.jieaudio.player.JieAudioPlayer;
import com.simpledemo.R;
import com.toastutil.toastutil.ToastUtil;

public class AudioActivity extends AppCompatActivity {
    private Button button_need;
   // private String url = "http://isure.stream.qqmusic.qq.com/C4000046HQLb22uXSN.m4a?guid=788682132&vkey=5220BE70861A5F7A5914E9ADD47B49397D579A32AA3635B539609BA957804996D5442613BF4E3B624CF6EE0B7B9D8156C44D0D4295FE805D&uin=0&fromtag=66";
    private String url = "http://220.194.121.158/amobile.music.tc.qq.com/C4000007ZqF126EfLu.m4a?guid=9029014292&vkey=9497D56C13C008B7BE14426FD8BD23712B9D686F09E97976E4255939B842DE68CD20CCA412B58570BD09E111E3DCE93A8EAE562CA25A0E0D&uin=0&fromtag=66";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio);
        JieAudioPlayer.getInstance().init(this);
        button_need = findViewById(R.id.button_need);
        button_need.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(JieAudioPlayer.getInstance().isPlaying()){
                    JieAudioPlayer.getInstance().pause();
                }else if(JieAudioPlayer.getInstance().hasPause()) {
                    JieAudioPlayer.getInstance().resume();
                }else {
                    JieAudioPlayer.getInstance().start(url,listener);
                }

            }
        });

    }

    private OnPlayStatusListener listener=new OnPlayStatusListener() {
        @Override
        public void onPrepared(MediaPlayer mp) {
        }

        @Override
        public void onCompletion(MediaPlayer mp) {

        }

        @Override
        public void isPlaying() {
            button_need.setText("播放ing");
        }


        @Override
        public void onBufferingUpdate(MediaPlayer mp, int percent) {
        }

        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            return false;
        }

        @Override
        public void onCodeErrorException(String exception) {
            ToastUtil.show(AudioActivity.this,exception);
        }

        @Override
        public boolean onInfo(MediaPlayer mp, int what, int extra) {
            return false;
        }

        @Override
        public void pause() {
            button_need.setText("暂停");
        }

        @Override
        public void resume() {
            button_need.setText("播放ing");
        }

        @Override
        public void wantPlay() {
            button_need.setText("播放ing");
        }
    };
}
