package com.simpledemo;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RemoteViews;

public class MainActivity extends AppCompatActivity {

    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "chat";
            String channelName = "聊天消息";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            createNotificationChannel(channelId, channelName, importance);

            channelId = "subscribe";
            channelName = "订阅消息";
            importance = NotificationManager.IMPORTANCE_DEFAULT;
            createNotificationChannel(channelId, channelName, importance);
            //sendSubscribeMsg();
            sendBigTextMsg();
        }else {
            sendSubscribeMsg2();
        }
    }

    private void sendBigTextMsg() {
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "subscribe")
                //.setCustomContentView(remoteView)
                // .setContentTitle("收到一条订阅消息")
                .setContentText("")
                // .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                // .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setAutoCancel(true);
        //创建大文本样式
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle("BigContentTitle")
                .setSummaryText("SummaryText")
                .bigText("回到通知栏上也是一样，每个开发者都只想着尽可能地去宣传自己的App，最后用户的手机就乱得跟鸡窝一样了。但是通知栏又还是有用处的，比如我们收到微信、短信等消息的时候，确实需要通知栏给我们提醒。因此分析下来，通知栏目前最大的问题就是，无法让用户对！");

        builder.setStyle(bigTextStyle); //设置大文本样式

        Notification notification = builder.build();
        manager.notify(100, notification);
    }


    @TargetApi(Build.VERSION_CODES.O)
    private void createNotificationChannel(String channelId, String channelName, int importance) {
        NotificationChannel channel = new NotificationChannel(channelId, channelName, importance);
        NotificationManager notificationManager = (NotificationManager) getSystemService(
                NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(channel);
    }


    public void sendChatMsg( ) {
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification notification = new NotificationCompat.Builder(this, "chat")
                .setContentTitle("收到一条聊天消息")
                .setContentText("今天中午吃什么？")
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setAutoCancel(true)
                .build();
        manager.notify(1, notification);
    }


    public void sendSubscribeMsg2( ) {
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        RemoteViews remoteView = new RemoteViews(getPackageName(), R.layout.notifycation_layout);//通知栏布局
        Notification notification = new NotificationCompat.Builder(this)
                //.setContentTitle("IG牛逼")
                //.setContentText("回到通知栏上也是一样，每个开发者都只想着尽可能地去宣传自己的App，最后用户的手机就乱得跟鸡窝一样了。但是通知栏又还是有用处的，比如我们收到微信、短信等消息的时候，确实需要通知栏给我们提醒。因此分析下来，通知栏目前最大的问题就是，无法让用户对！")
                .setCustomContentView(remoteView)
                 .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                // .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setAutoCancel(true)
                .build();
        manager.notify(2, notification);
    }

    public void sendSubscribeMsg( ) {
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        RemoteViews remoteView = new RemoteViews(getPackageName(), R.layout.notifycation_layout);//通知栏布局
        Notification notification = new NotificationCompat.Builder(this, "subscribe")
                .setCustomContentView(remoteView)
                //.setCustomContentView(remoteView)
                // .setContentTitle("收到一条订阅消息")
               .setContentText("回到通知栏上也是一样，每个开发者都只想着尽可能地去宣传自己的App，最后用户的手机就乱得跟鸡窝一样了。但是通知栏又还是有用处的，比如我们收到微信、短信等消息的时候，确实需要通知栏给我们提醒。因此分析下来，通知栏目前最大的问题就是，无法让用户对！")
               // .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
               // .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                  .setAutoCancel(true)
                .build();
        manager.notify(2, notification);
    }


}
