package com.simpledemo;

import android.app.Application;

import com.toastutil.toastutil.ToastUtil;

/**
 * Description:
 * Date：2018/11/12-9:20
 * Author: Liangchaojie
 */
public class APP extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ToastUtil.init(this);
    }
}
