package com.universallauch.network;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Description: Retrofit公有参数
 * Date：2018/9/28-6:11
 * Author: Liangchaojie
 */
public class CommonInterceptor implements Interceptor {
    public CommonInterceptor() { }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request oldRequest = chain.request();
        // 添加新的参数
        HttpUrl.Builder authorizedUrlBuilder = oldRequest.url()
                .newBuilder()
                .scheme(oldRequest.url().scheme())
                .host(oldRequest.url().host())
                .addQueryParameter("client_token", "")
                .addQueryParameter("token_fr", "")
                .addQueryParameter("fc_v", "3.0.4")
                .addQueryParameter("wb_actoken", "")
                .addQueryParameter("fr", "lcs_client_caidao_android")
                .addQueryParameter("appid", "")
                .addQueryParameter("debug", "1")
                .addQueryParameter("channel", "sina_android");

        // 新的请求
        Request newRequest = oldRequest.newBuilder()
                .method(oldRequest.method(), oldRequest.body())
                .url(authorizedUrlBuilder.build())
                .build();

        return chain.proceed(newRequest);
    }
}
