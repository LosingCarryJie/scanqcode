package com.universallauch.network;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Description:
 * Date：2018/9/28-5:56
 * Author: Liangchaojie
 */
public class NetUtils {
    public static final String BASE_URL = "http://licaishi.sina.com.cn/";

    public static IService getService(){
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(new CommonInterceptor());

        OkHttpClient httpClient = client.build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();
        return retrofit.create(IService.class);
    }
}
