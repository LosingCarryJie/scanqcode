package com.universallauch.network;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Description:
 * Date：2018/9/28-5:55
 * Author: Liangchaojie
 */
public interface IService {
    @GET("apic1/recommentIndexNew")
    Call<Object> getUpdateInfo();
}
