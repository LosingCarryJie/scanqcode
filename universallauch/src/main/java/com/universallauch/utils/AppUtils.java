package com.universallauch.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Description:  操作App的工具类，用于获取App的信息，卸载App等
 * Date：2018/8/16-15:44
 * Author: Liangchaojie
 */
public class AppUtils {

    /**
     * 获取所有已经安装的App的包名
     * @param context
     */
    public static List<String> getExistAppPackageName(Context context) {
        List<String> stringList = new ArrayList<>();
        final PackageManager packageManager = context.getPackageManager();
        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        final List<ResolveInfo> apps = packageManager.queryIntentActivities(mainIntent, 0);

        String manu = android.os.Build.MANUFACTURER;
        String manuDa = android.os.Build.MANUFACTURER.toUpperCase();
        String manuXiao = android.os.Build.MANUFACTURER.toLowerCase();
        for (int i = 0; i < apps.size(); i++) {
            String name = apps.get(i).activityInfo.packageName;
            stringList.add(name);
        }
        return stringList;
    }


    /**
     * 获取App名字
     * @param context
     * @param pakgename
     * @return
     */
    public static String getAppName(Context context,String pakgename) {
        ApplicationInfo appInfo;
        PackageManager pm = context.getPackageManager();
        try {
            appInfo = pm.getApplicationInfo(pakgename, PackageManager.GET_META_DATA);
            String name = String.valueOf(pm.getApplicationLabel(appInfo));
            return name;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "应用名解析异常";
    }


    /**
     * 获取App名字
     * @param context
     * @param pakgename
     * @return
     */
    public static Drawable getAppIcon(Context context, String pakgename) {
        Drawable drawable;
        PackageManager pm = context.getPackageManager();
        try {
            drawable = pm.getApplicationIcon(pakgename);
            return drawable;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * 根据包名启动某个App
     * @param context
     * @param packageName
     */
    public static void launchAppByPackageName(Context context,String packageName) {
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        // 这里如果intent为空，就说名没有安装要跳转的应用嘛
        if (intent != null) {
            // 这里跟Activity传递参数一样的嘛，不要担心怎么传递参数，还有接收参数也是跟Activity和Activity传参数一样
            intent.putExtra("name", "liangchaojie");
            intent.putExtra("birthday", "1994-06-18");
            context.startActivity(intent);
        } else {
            // 没有安装要跳转的app应用，提醒一下
            Toast.makeText(context.getApplicationContext(), "哟，赶紧下载安装这个APP吧", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * 卸载指定包名的应用
     * @param packageName
     */
    public static boolean uninstallAppByPackageName(Context context,String packageName) {
        boolean b = checkApplication(context,packageName);
        if (b) {
            Uri packageURI = Uri.parse("package:".concat(packageName));
            Intent intent = new Intent(Intent.ACTION_DELETE);
            intent.setData(packageURI);
            context.startActivity(intent);
            return true;
        }
        return false;
    }

    /**
     * 判断该包名的应用是否安装
     *
     * @param packageName
     * @return
     */
    private static boolean checkApplication(Context context,String packageName) {
        if (packageName == null || "".equals(packageName)) {
            return false;
        }
        try {
            context.getPackageManager().getApplicationInfo(packageName,
                    PackageManager.MATCH_UNINSTALLED_PACKAGES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }
}
