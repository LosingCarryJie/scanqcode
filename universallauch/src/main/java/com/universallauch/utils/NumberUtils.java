package com.universallauch.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Description:
 * Date：2018/8/17-12:45
 * Author: Liangchaojie
 */
public class NumberUtils {
    public static  int[] getNewArray(int[] array,int m){
        if(array==null) return null;
        List<Integer> integerList = new ArrayList<>();
        for (int i = 0; i <array.length ; i++) {
            integerList.add(array[i]+m);
            integerList.add(array[i]-m);
        }

        for (int i = 0; i < integerList.size(); i++) {
            if(integerList.get(i)>100||integerList.get(i)<0){
                integerList.remove(i);
            }
        }

        int[] result = new int[integerList.size()];
        for (int i = 0; i < integerList.size(); i++) {
            result[i] = integerList.get(i);
        }

        return result;
    }
}
