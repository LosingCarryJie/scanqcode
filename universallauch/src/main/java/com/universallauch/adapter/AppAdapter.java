package com.universallauch.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.universallauch.R;
import com.universallauch.bean.NameBean;
import com.universallauch.utils.AppUtils;

import java.util.List;

/**
 * Description:
 * Date：2018/8/16-16:24
 * Author: Liangchaojie
 */
public class AppAdapter extends RecyclerView.Adapter<AppAdapter.VH> {
    public AppAdapter(Context context, List<NameBean> mList) {
        this.context = context;
        this.mList = mList;
    }

    private Context context;
    private List<NameBean> mList;

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_app, parent, false);
        VH v = new VH(view);
        return v;
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        final NameBean bean = mList.get(position);
         holder.mTvAppname.setText(bean.nameChinese);
         holder.mImgAppicon.setImageDrawable(bean.icon);
         holder.mBtnOpen.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 AppUtils.launchAppByPackageName(context,bean.getPackName());
             }
         });
         holder.mBtnDelete.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 AppUtils.uninstallAppByPackageName(context, bean.getPackName());

             }
         });

        String word = mList.get(position).firstLetter;
        holder.tv_word.setText(word);
        //将相同字母开头的合并在一起
        if (position == 0) {
            //第一个是一定显示的
            holder.tv_word.setVisibility(View.VISIBLE);
        } else {
            //后一个与前一个对比,判断首字母是否相同，相同则隐藏
            String headerWord = mList.get(position - 1).firstLetter;
            if (word.equals(headerWord)) {
                holder.tv_word.setVisibility(View.GONE);
            } else {
                holder.tv_word.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mList==null?0:mList.size();
    }



    public class VH extends RecyclerView.ViewHolder {
        public ImageView mImgAppicon;
        public TextView mTvAppname;
        public TextView tv_word;
        public Button mBtnDelete;
        public Button mBtnOpen;
        public VH(View itemView) {
            super(itemView);
            initView(itemView);
        }

        private void initView(View itemView) {
            mImgAppicon = itemView.findViewById(R.id.img_appicon);
            mTvAppname = itemView.findViewById(R.id.tv_appname);
            mBtnDelete = itemView.findViewById(R.id.btn_delete);
            mBtnOpen = itemView.findViewById(R.id.btn_open);
            tv_word = itemView.findViewById(R.id.tv_word);
        }
    }
}
