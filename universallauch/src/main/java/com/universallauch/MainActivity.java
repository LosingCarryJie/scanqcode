package com.universallauch;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Toast;

import com.universallauch.adapter.AppAdapter;
import com.universallauch.bean.NameBean;
import com.universallauch.utils.AppUtils;
import com.universallauch.utils.ChineseSortUtil;
import com.universallauch.utils.NumberUtils;
import com.universallauch.view.LetterIndexView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements LetterIndexView.onWordsChangeListener{

    private RecyclerView mRecyclerview;
    private AppAdapter appAdapter;
    private List<NameBean> mlist;
    private LetterIndexView mWords;
    private LinearLayoutManager mLayoutManager;
    @Override
    protected void onResume() {
        super.onResume();
        initData();
        if (appAdapter != null) {
            appAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initData();
        doLogic();

        int[] a = new int[]{4,7,16,20,24,30};
        int[] b=NumberUtils.getNewArray(a, 11);
        for (int i = 0; i < b.length; i++) {
            System.out.println(b[i]+" ");
        }
    }


    private void doLogic() {
        appAdapter = new AppAdapter(MainActivity.this, mlist);
         mLayoutManager = new LinearLayoutManager(MainActivity.this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);//尤其是这句
        mRecyclerview.setLayoutManager(mLayoutManager);
        mRecyclerview.setAdapter(appAdapter);
    }

    private void initData() {
        if (mlist != null) {
            mlist.clear();
        } else {
            mlist = new ArrayList<>();
        }
        List<String> packList = AppUtils.getExistAppPackageName(this);
        for (int i = 0; i < packList.size(); i++) {
            NameBean appBean = new NameBean(AppUtils.getAppName(this, packList.get(i)));
            appBean.setPackName(packList.get(i));
            appBean.setIcon(AppUtils.getAppIcon(this, packList.get(i)));
            mlist.add(appBean);
        }
        ChineseSortUtil.sortList(mlist);//对集合进行排序
        for (int i = 0; i < mlist.size(); i++) {
            Log.i("TAG", "initData: "+AppUtils.getAppName(this,mlist.get(i).getPackName()));
        }
    }

    private void initView() {
        mRecyclerview = findViewById(R.id.recyclerview);
        mWords = findViewById(R.id.words);

        mWords.setOnWordsChangeListener(this);
        mRecyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //当滑动列表的时候，更新右侧字母列表的选中状态
                mWords.setTouchIndex(mlist.get( mLayoutManager.findFirstVisibleItemPosition()).firstLetter);
            }
        });
    }

    @Override
    public void wordsChange(String words) {
        updateListView(words);
    }

    /**
     * 选中了字母索引列表的任何一个字母，ListView相应的字母也得置顶
     * @param words
     */
    private void updateListView(String words) {
        for(int i=0;i<mlist.size();i++){
            if(words.equals(mlist.get(i).firstLetter)||words.toLowerCase().equals(mlist.get(i).firstLetter)){
                mRecyclerview.scrollToPosition(i);
                mLayoutManager.scrollToPositionWithOffset(i, 0);
                return;
            }
        }
    }

}
