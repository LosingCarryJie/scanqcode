package com.universallauch;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.universallauch.network.NetUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetWorkActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_net_work);
        doLogic();
    }

    private void doLogic() {
        Call call = NetUtils.getService().getUpdateInfo();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Toast.makeText(NetWorkActivity.this,"请求成功!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Toast.makeText(NetWorkActivity.this,"请求失败!",Toast.LENGTH_SHORT).show();

            }
        });
    }
}
